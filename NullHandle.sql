Create table null_handle
(
Emp_id int,
Emp_name varchar(20),
Salary int
)

insert into null_handle values (1,'Adi',15000),(2,'bac',12000),(3,'Bo',30000),(4,'bean',null),(5,'joe',17000),(6,'jack',null),(7,'cin',22000);


select Emp_id , Emp_name from null_handle
where salary IS NULL; 

select Emp_id , Emp_name from null_handle
where salary IS NOT NULL;
  
select Emp_id , Emp_name , ISNULL(Salary,0)as salary from null_handle;

select Emp_id , Emp_name , COALESCE(Salary,0)as salary from null_handle;

select Emp_id , Emp_name , NULLIF(Salary,12000)as salary from null_handle; --NullIF gives null if value of both parameter is same.

DECLARE @a VARCHAR(5)='Hello',  
@b INT =5,  
@c int = null,
@d int = 6
SELECT ISNULL(@a, @d) AS ISNULLResult --For isnull it's data type is determined by first input.
SELECT COALESCE(@d, @a) AS COALESCEResult--For Coalesce it's data type is determined by returned element.

declare @test varchar(5)  
select isnull(@test, 'ABCDEFG') AS ISNULLResult  --Isnull looks at first parameter so length of second parameter is limited.
select coalesce(@test, 'ABCDEFG') AS coalesceResult  --coalesce doesn't has this limitation of size.