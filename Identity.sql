Create Table Employee 
(
id int identity(1,1) primary key NOT NULL,
EmpName varchar(20) NULL
);

insert into Employee(EmpName) values ('Aarav'),('Abhi'),('joe'),('Fin'),('Jack'),('Tom'),('Josh'),('pal'),('Tus'),('Zack');

Create Table Deptt 
(
id int identity(1,1) primary key,
Dept_Name varchar(20) Null
);

insert into deptt(Dept_name) values ('IT'),('HR'),('Services');

Create trigger trig on Employee
for insert as
Begin
insert into Deptt values('')
End

insert into Employee values ('shaun');

Select * from Employee;
Select * from deptt;

Select @@identity as MaxUsedIdentity; --Gives the last identity generated in the current session. 
select Scope_identity() as [Scopeidentity]; --Gives the last identity generated in the same scope.

--user1

insert into deptt values('tus');

select IDENT_CURRENT('deptt') as [CurrentIdentity]; -- Gives the last identity value for a given table under any connection/user.