use sample;
create table emp
(
Emp_id int,
[Name] varchar(20),
Salary int
)
select distinct * from emp order by salary desc;

insert into emp values (1,'Alex',1200),(2,'Bae',35000),(3,'chetan',40000),(4,'ayu',25000),(5,'tus',27000);

alter procedure n_highest_salary  @n int
as
begin
select top 1 * from (select Distinct top (@n) * from emp order by salary desc) as Employee order by salary ;
end

n_highest_salary @n = 3;