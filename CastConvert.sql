/* Cast Function is Used to Convert varible of one data type to another data type
 Syntax: Cast(Variable_name as DataType) */


Declare @A int = 25;
Declare @B int = 34;
Select cast(@A as varchar(2)) + cast(@B as varchar(2))as [Concat];

Declare @C varchar(2) = 25;
Declare @D varchar(2) = 34;
Select cast(@C as int) + cast(@D as int)as [Sum];

Select Cast(CURRENT_TIMESTAMP as Date) as [Date];
Select Cast(CURRENT_TIMESTAMP as Time) as [Time];
select Cast(getdate() as date) as [date];

/* Convert Function used to convert a value of any data type into a specific data type, we can also provide some specific style to output
   Syntax: Convert(data_type , Expression , Style) */

Select Convert(int, 5.672) as integer_value;

Select Convert(Varchar, getdate(), 0) as[dd.mm.yy];
