Declare @CurrentDateTime datetime
Set @CurrentDateTime = Getdate()
Select Convert(Varchar, @CurrentDateTime, 1) As [MM/DD/YYYY]

Select Convert(Varchar, Getdate(), 2) As [YY.MM.DD]
  
Create table DateTimeFormats
(id int, 
[DateTime] Varchar(30)
)

Declare @id int = 1
While (@id <= 10)
	Begin
		insert into DateTImeFormats values( @id , Convert(Varchar, Getdate(), @id ))
		Set @id = @id + 1
	End

Select * from DateTimeFormats;

DECLARE @d DATE = Getdate();
SELECT FORMAT( @d, 'd', 'en-US' ) 'US English'  
      ,FORMAT( @d, 'd', 'en-gb' ) 'Great Britain English'  
      ,FORMAT( @d, 'd', 'de-de' ) 'German'  
      ,FORMAT( @d, 'd', 'zh-cn' ) 'Simplified Chinese (PRC)';  
  
SELECT FORMAT( @d, 'D', 'en-US' ) 'US English'  
      ,FORMAT( @d, 'D', 'en-gb' ) 'Great Britain English'  
      ,FORMAT( @d, 'D', 'de-de' ) 'German'  
      ,FORMAT( @d, 'D', 'zh-cn' ) 'Chinese (Simplified PRC)';  